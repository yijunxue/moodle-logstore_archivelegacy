@logstore @logstore_archivelegacy @uon
Feature: Manage standard log archive settings
    In order to maintain the log store
    As an administrator
    I need to be able to configure the settings on the log store.

    Scenario: Change the log store settings.
        Given Legacy log archive is enabled
        And I log in as "admin"
        And I navigate to "Plugins > Logging > Legacy log archive" in site administration
        And I set the following fields to these values:
            | Choose database driver | Improved MySQL (native/mysqli) |
            | Database host | example.com |
            | Database user | moodle |
            | Database password | p@ssw0rd |
            | Database name | archive |
            | Database table | archive_logstore_standard_log |
            | Persistent database connections | 1 |
            | Unix socket | /var/run/mysqld/mysqld.sock |
            | Database port | 667 |
            | Database schema | archiveschema |
            | Database collation | utf-8 |
            | id_s_logstore_archivelegacy_archiveafterv | 179 |
            | id_s_logstore_archivelegacy_archiveafteru | days |
            | id_s_logstore_archivelegacy_archivelifetimev | 730 |
            | id_s_logstore_archivelegacy_archivelifetimeu | days |
        When I press "Save changes"
        And I wait to be redirected
        And I navigate to "Plugins > Logging > Legacy log archive" in site administration
        Then the following fields match these values:
            | Choose database driver | Improved MySQL (native/mysqli) |
            | Database host | example.com |
            | Database user | moodle |
            | Database password | p@ssw0rd |
            | Database name | archive |
            | Database table | archive_logstore_standard_log |
            | Persistent database connections | 1 |
            | Unix socket | /var/run/mysqld/mysqld.sock |
            | Database port | 667 |
            | Database schema | archiveschema |
            | Database collation | utf-8 |
            | id_s_logstore_archivelegacy_archiveafterv | 179 |
            | id_s_logstore_archivelegacy_archiveafteru | days |
            | id_s_logstore_archivelegacy_archivelifetimev | 730 |
            | id_s_logstore_archivelegacy_archivelifetimeu | days |
<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Legacy log archive store language strings.
 *
 * @package    logstore_archivelegacy
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2015 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['archiveafter'] = 'Archive after';
$string['archivelifetime'] = 'Archive lifetime';
$string['configarchiveafter'] = 'This setting specifies the length of time that logs will remain in the legacy log before they are moved to the archive log. This time should be set to be shorter than the log life time of the legacy log.';
$string['configarchivelifetime'] = 'This specifies the length of time you want to keep logs about user activity. Logs that have been in the archive longer than this time are automatically deleted.';
$string['databasesettings'] = 'Database settings';
$string['databasesettings_help'] = 'Connection details for the external log database: {$a}';
$string['databasepersist'] = 'Persistent database connections';
$string['databaseschema'] = 'Database schema';
$string['databasecollation'] = 'Database collation';
$string['databasetable'] = 'Database table';
$string['databasetable_help'] = 'Name of the table where logs will be stored. This table should have a structure identical to the one used by logstore_legacy (mdl_log).';
$string['neverarchive'] = 'Never archive';
$string['pluginname'] = 'Legacy log archive';
$string['pluginname_desc'] = 'A log plugin that stores an archive of legacy log entries in an external database table.';
$string['privacy:metadata:log'] = 'A collection of past events archived from the legacy log';
$string['privacy:metadata:log:action'] = 'A description of the action';
$string['privacy:metadata:log:info'] = 'Additional information';
$string['privacy:metadata:log:ip'] = 'The IP address used at the time of the event';
$string['privacy:metadata:log:time'] = 'The time when the action took place';
$string['privacy:metadata:log:url'] = 'The URL related to the event';
$string['privacy:metadata:log:userid'] = 'The ID of the user who performed the action';
$string['taskarchive'] = 'Archive logs';
$string['taskcleanup'] = 'Cleanup logs';
$string['testsettings'] = 'Test connection';
$string['testingsettings'] = 'Testing database settings...';

<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace logstore_archivelegacy\privacy;

use core_privacy\local\metadata\collection;
use core_privacy\local\request\approved_contextlist;
use core_privacy\local\request\approved_userlist;
use core_privacy\local\request\contextlist;
use core_privacy\local\request\userlist;
use core_privacy\local\request\transform;
use core_privacy\local\request\writer;
use tool_log\local\privacy\helper;

defined('MOODLE_INTERNAL') || die();

/**
 * Privacy provider for the archive legacy log store.
 *
 * @package    logstore_archivelegacy
 * @subpackage privacy
 * @copyright  2018 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class provider implements
    \core_privacy\local\metadata\provider,
    \tool_log\local\privacy\logstore_provider,
    \tool_log\local\privacy\logstore_userlist_provider {

    /**
     * Returns metadata.
     *
     * @param collection $collection The initialised collection to add items to.
     * @return collection A listing of user data stored through this system.
     */
    public static function get_metadata(collection $collection) : collection {
        // Does not use a moodle subsystem.
        // No personal data stored in the database.
        // Exports data from Moodle to another system.
        $log = array(
            'time' => 'privacy:metadata:log:time',
            'userid' => 'privacy:metadata:log:userid',
            'ip' => 'privacy:metadata:log:ip',
            'action' => 'privacy:metadata:log:action',
            'url' => 'privacy:metadata:log:url',
            'info' => 'privacy:metadata:log:info',
        );
        $collection->add_external_location_link('log', $log, 'privacy:metadata:log');
        // Does not store any user preferences.
        return $collection;
    }

    /**
     * Add contexts that contain user information for the specified user.
     *
     * @param contextlist $contextlist The contextlist to add the contexts to.
     * @param int $userid The user to find the contexts for.
     * @return void
     * @global \moodle_database $DB
     */
    public static function add_contexts_for_userid(contextlist $contextlist, $userid) {
        global $DB;
        list($db, $table) = static::get_database_and_table();
        if (!$db || !$table) {
            // Plugin is not configured.
            return;
        }

        // Find the records for the user from the external database.
        $unique = $db->sql_concat('course', "'-'", 'cmid');
        $select = "DISTINCT $unique, course, cmid";
        $sql = "userid = :userid";
        $params = ['userid' => $userid];
        $records = $db->get_records_select($table, $sql, $params, '', $select);
        if (empty($records)) {
            return;
        }

        // Find the context levels for each record.
        $coursecontexts = array();
        $modulecontexts = array();
        $systemcontext = false;
        foreach ($records as $record) {
            if ($record->cmid > 0) {
                $modulecontexts[] = $record->cmid;
            } else if ($record->course > 0) {
                $coursecontexts[] = $record->course;
            } else {
                $systemcontext = true;
            }
        }

        // Build a query we can run in Moodle to find the context ids.
        $contextparams = array(
            'courselevel' => CONTEXT_COURSE,
            'modulelevel' => CONTEXT_MODULE,
        );
        list($courses, $courseparams) = $DB->get_in_or_equal($coursecontexts, SQL_PARAMS_NAMED, 'mod');
        list($mods, $modparams) = $DB->get_in_or_equal($modulecontexts, SQL_PARAMS_NAMED, 'mod');
        $contextsql = array(
            "SELECT id FROM {context} WHERE instanceid $mods AND contextlevel = :modulelevel",
            "SELECT id FROM {context} WHERE instanceid $courses AND contextlevel = :courselevel",
        );
        if ($systemcontext) {
            $contextsql[] = "SELECT id FROM {context} WHERE id = :syscontextid";
            $contextparams['syscontextid'] = SYSCONTEXTID;
        }
        $queryparams = $contextparams + $courseparams + $modparams;

        $contextlist->add_from_sql(implode(' UNION ', $contextsql), $queryparams);
    }

    /**
     * Add user IDs that contain user information for the specified context.
     *
     * @param \core_privacy\local\request\userlist $userlist The userlist to add the users to.
     * @return void
     */
    public static function add_userids_for_context(userlist $userlist) {
        list($db, $table) = static::get_database_and_table();
        if (!$db || !$table) {
            // Plugin is not configured.
            return;
        }
        $context = $userlist->get_context();
        list($insql, $params) = static::get_sql_where_from_contexts([$context]);
        $userids = $db->get_fieldset_select($table, 'DISTINCT userid', $insql, $params);
        $userlist->add_users($userids);
    }

    /**
     * Export all user data for the specified user, in the specified contexts.
     *
     * @param approved_contextlist $contextlist The approved contexts to export information for.
     */
    public static function export_user_data(approved_contextlist $contextlist) {
        list($db, $table) = static::get_database_and_table();
        if (!$db || !$table) {
            // Plugin is not configured.
            return;
        }

        $userid = $contextlist->get_user()->id;
        list($insql, $inparams) = static::get_sql_where_from_contexts($contextlist->get_contexts());
        if (empty($insql)) {
            return;
        }
        $sql = "userid = :userid AND $insql";
        $params = array_merge($inparams, ['userid' => $userid]);

        $path = [get_string('privacy:path:logs', 'tool_log'), get_string('pluginname', 'logstore_archivelegacy')];
        $flush = function($lastcontextid, $data) use ($path) {
            $context = \context::instance_by_id($lastcontextid);
            writer::with_context($context)->export_data($path, (object) ['logs' => $data]);
        };

        $lastcontextid = null;
        $data = [];
        $recordset = $db->get_recordset_select($table, $sql, $params, 'course, cmid, time, id');
        foreach ($recordset as $record) {
            $event = \logstore_legacy\event\legacy_logged::restore_legacy($record);
            $context = $event->get_context();
            if ($lastcontextid && $lastcontextid != $context->id) {
                $flush($lastcontextid, $data);
                $data = [];
            }

            $extra = $event->get_logextra();
            $data[] = [
                'name' => $event->get_name(),
                'description' => $event->get_description(),
                'timecreated' => transform::datetime($event->timecreated),
                'ip' => $extra['ip'],
                'origin' => helper::transform_origin($extra['origin']),
            ];

            $lastcontextid = $context->id;
        }
        if ($lastcontextid) {
            $flush($lastcontextid, $data);
        }
        $recordset->close();
    }

    /**
     * Delete all data for all users in the specified context.
     *
     * @param \context $context The specific context to delete data for.
     */
    public static function delete_data_for_all_users_in_context(\context $context) {
        list($db, $table) = static::get_database_and_table();
        if (!$db || !$table) {
            // Plugin is not configured.
            return;
        }
        list($sql, $params) = static::get_sql_where_from_contexts([$context]);
        if (empty($sql)) {
            return;
        }
        $db->delete_records_select($table, $sql, $params);
    }

    /**
     * Delete all user data for the specified user, in the specified contexts.
     *
     * @param approved_contextlist $contextlist The approved contexts and user information to delete information for.
     */
    public static function delete_data_for_user(approved_contextlist $contextlist) {
        list($db, $table) = static::get_database_and_table();
        if (!$db || !$table) {
            // Plugin is not configured.
            return;
        }
        list($sql, $params) = static::get_sql_where_from_contexts($contextlist->get_contexts());
        if (empty($sql)) {
            return;
        }
        $userid = $contextlist->get_user()->id;
        $db->delete_records_select($table, "$sql AND userid = :userid", array_merge($params, ['userid' => $userid]));
    }

    /**
     * Delete all data for a list of users in the specified context.
     *
     * @param \core_privacy\local\request\approved_userlist $userlist The specific context and users to delete data for.
     * @return void
     */
    public static function delete_data_for_userlist(approved_userlist $userlist) {
        list($db, $table) = static::get_database_and_table();
        if (!$db || !$table) {
            // Plugin is not configured.
            return;
        }
        list($sql, $params) = static::get_sql_where_from_contexts([$userlist->get_context()]);
        if (empty($sql)) {
            return;
        }
        list($usersql, $userparams) = $db->get_in_or_equal($userlist->get_userids(), SQL_PARAMS_NAMED);
        $params = array_merge($params, $userparams);
        $db->delete_records_select($table, "$sql AND userid $usersql", $params);
    }

    /**
     * Get the database object.
     *
     * @return array Containing moodle_database, string, or null values.
     */
    protected static function get_database_and_table() {
        $manager = get_log_manager();
        $store = new \logstore_archivelegacy\log\store($manager);
        $db = $store->get_extdb();
        return $db ? [$db, $store->get_config_value('dbtable')] : [null, null];
    }

    /**
     * Get an SQL where statement from a list of contexts.
     *
     * @param array $contexts The contexts.
     * @return array [$sql, $params]
     */
    protected static function get_sql_where_from_contexts(array $contexts) {
        list($db, $table) = static::get_database_and_table();
        if (!$db || !$table) {
            // Plugin is not configured.
            return [null, null];
        }

        $sorted = array_reduce($contexts, function ($carry, $context) {
            $level = $context->contextlevel;
            if ($level == CONTEXT_MODULE || $level == CONTEXT_COURSE) {
                $carry[$level][] = $context->instanceid;
            } else if ($level == CONTEXT_SYSTEM) {
                $carry[$level] = $context->id;
            }
            return $carry;
        }, [
            CONTEXT_COURSE => [],
            CONTEXT_MODULE => [],
            CONTEXT_SYSTEM => null,
        ]);

        $sqls = [];
        $params = [];

        if (!empty($sorted[CONTEXT_MODULE])) {
            list($insql, $inparams) = $db->get_in_or_equal($sorted[CONTEXT_MODULE], SQL_PARAMS_NAMED);
            $sqls[] = "cmid $insql";
            $params = array_merge($params, $inparams);
        }

        if (!empty($sorted[CONTEXT_COURSE])) {
            list($insql, $inparams) = $db->get_in_or_equal($sorted[CONTEXT_COURSE], SQL_PARAMS_NAMED);

            $sqls[] = "cmid = 0 AND course $insql";
            $params = array_merge($params, $inparams);
        }

        if (!empty($sorted[CONTEXT_SYSTEM])) {
            $sqls[] = "course <= 0";
        }

        if (empty($sqls)) {
            return [null, null];
        }

        return ['((' . implode(') OR (', $sqls) . '))', $params];
    }
}

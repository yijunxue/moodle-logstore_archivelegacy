<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Legacy log archive log store.
 *
 * @package    logstore_archivelegacy
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2015 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace logstore_archivelegacy\log;
defined('MOODLE_INTERNAL') || die();
use logstore_legacy\event\legacy_logged;

/**
 * Legacy log archive log store.
 *
 * Based on the External database and Legacy log stores
 * by Petr Skoda {@link http://skodak.org}
 *
 * @package    logstore_archivelegacy
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2015 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class store implements \tool_log\log\store, \core\log\sql_reader {
    use \tool_log\helper\store,
        \tool_log\helper\reader,
        \logstore_archivelegacy\helper\externaldb;

    /**
     * Construct
     *
     * @param \tool_log\log\manager $manager
     */
    public function __construct(\tool_log\log\manager $manager) {
        $this->helper_setup($manager);
    }

    /** @var array list of db fields which needs to be replaced for legacy log query */
    protected static $standardtolegacyfields = array(
        'timecreated'       => 'time',
        'courseid'          => 'course',
        'contextinstanceid' => 'cmid',
        'origin'            => 'ip',
        'anonymous'         => 0,
    );

    /** @var string Regex to replace the crud params */
    const CRUD_REGEX = "/(crud).*?(<>|=|!=).*?'(.*?)'/s";

    /**
     * This method contains mapping required for Moodle core to make legacy store compatible with other sql_select_reader based
     * queries.
     *
     * @param string $selectwhere Select statment
     * @param array $params params for the sql
     * @param string $sort sort fields
     *
     * @return array returns an array containing the sql predicate, an array of params and sorting parameter.
     */
    protected static function replace_sql_legacy($selectwhere, array $params, $sort = '') {
        // Following mapping is done to make can_delete_course() compatible with legacy store.
        if ($selectwhere == "userid = :userid AND courseid = :courseid AND eventname = :eventname AND timecreated > :since" and
                empty($sort)) {
            $replace = "module = 'course' AND action = 'new' AND userid = :userid AND url = :url AND time > :since";
            $params += array('url' => "view.php?id={$params['courseid']}");
            return array($replace, $params, $sort);
        }

        // Replace db field names to make it compatible with legacy log.
        foreach (self::$standardtolegacyfields as $from => $to) {
            $selectwhere = str_replace($from, $to, $selectwhere);
            if (!empty($sort)) {
                $sort = str_replace($from, $to, $sort);
            }
            if (isset($params[$from])) {
                $params[$to] = $params[$from];
                unset($params[$from]);
            }
        }

        // Replace crud fields.
        $selectwhere = preg_replace_callback(self::CRUD_REGEX, 'self::replace_crud', $selectwhere);

        return array($selectwhere, $params, $sort);
    }

    /**
     * Get an array of events based on the passed on params.
     *
     * @param string $selectwhere select conditions.
     * @param array $params params.
     * @param string $sort sortorder.
     * @param int $limitfrom limit constraints.
     * @param int $limitnum limit constraints.
     *
     * @return array|\core\event\base[] array of events.
     */
    public function get_events_select($selectwhere, array $params, $sort, $limitfrom, $limitnum) {
        if (!$this->init()) {
            return array();
        }

        if (!$dbtable = $this->get_config('dbtable')) {
            return array();
        }

        $sort = self::tweak_sort_by_id($sort);

        // Replace the query with hardcoded mappings required for core.
        list($selectwhere, $params, $sort) = self::replace_sql_legacy($selectwhere, $params, $sort);

        try {
            $records = $this->extdb->get_records_select($dbtable, $selectwhere, $params, $sort, '*', $limitfrom, $limitnum);
        } catch (\moodle_exception $ex) {
            debugging("error converting legacy event data " . $ex->getMessage() . $ex->debuginfo, DEBUG_DEVELOPER);
            return array();
        }

        $events = array();
        foreach ($records as $data) {
            $events[$data->id] = $this->get_log_event($data);
        }

        return $events;
    }

    /**
     * Fetch records using given criteria returning a Traversable object.
     *
     * Note that the traversable object contains a moodle_recordset, so
     * remember that is important that you call close() once you finish
     * using it.
     *
     * @param string $selectwhere
     * @param array $params
     * @param string $sort
     * @param int $limitfrom
     * @param int $limitnum
     * @return \core\dml\recordset_walk|\core\event\base[]
     */
    public function get_events_select_iterator($selectwhere, array $params, $sort, $limitfrom, $limitnum) {
        if (!$this->init()) {
            return array();
        }
        if (!$dbtable = $this->get_config('dbtable')) {
            return array();
        }
        $sort = self::tweak_sort_by_id($sort);
        list($selectwhere, $params, $sort) = self::replace_sql_legacy($selectwhere, $params, $sort);
        try {
            $recordset = $this->extdb->get_recordset_select($dbtable, $selectwhere, $params, $sort, '*', $limitfrom, $limitnum);
        } catch (\moodle_exception $ex) {
            debugging("error converting legacy event data " . $ex->getMessage() . $ex->debuginfo, DEBUG_DEVELOPER);
            return array();
        }
        return new \core\dml\recordset_walk($recordset, array($this, 'get_log_event'));
    }

    /**
     * Returns an event from the log data.
     *
     * @param  \stdClass $data
     * @return \core\event\base
     */
    public function get_log_event($data) {
        return legacy_logged::restore_legacy($data);
    }

    /**
     * Get number of events present for the given select clause.
     *
     * @param string $selectwhere select conditions.
     * @param array $params params.
     *
     * @return int Number of events available for the given conditions
     */
    public function get_events_select_count($selectwhere, array $params) {
        if (!$this->init()) {
            return 0;
        }
        if (!$dbtable = $this->get_config('dbtable')) {
            return 0;
        }
        // Replace the query with hardcoded mappings required for core.
        list($selectwhere, $params) = self::replace_sql_legacy($selectwhere, $params);

        try {
            return $this->extdb->count_records_select($dbtable, $selectwhere, $params);
        } catch (\moodle_exception $ex) {
            debugging("error converting legacy event data " . $ex->getMessage() . $ex->debuginfo, DEBUG_DEVELOPER);
            return 0;
        }
    }

    /**
     * Are the new events appearing in the reader?
     *
     * @return bool true means new log events are being added, false means no new data will be added
     */
    public function is_logging() {
        return false;
    }

    /**
     * Generate a replace string for crud related sql conditions. This function is called as callback to preg_replace_callback()
     * on the actual sql.
     *
     * @param array $match matched string for the passed pattern
     *
     * @return string The sql string to use instead of original
     */
    protected static function replace_crud($match) {
        $return = '';
        unset($match[0]); // The first entry is the whole string.
        foreach ($match as $m) {
            // We hard code LIKE here because we are not worried about case sensitivity and want this to be fast.
            switch ($m) {
                case 'crud' :
                    $replace = 'action';
                    break;
                case 'c' :
                    switch ($match[2]) {
                        case '=' :
                            $replace = " LIKE '%add%'";
                            break;
                        case '!=' :
                        case '<>' :
                            $replace = " NOT LIKE '%add%'";
                            break;
                        default:
                            $replace = '';
                    }
                    break;
                case 'r' :
                    switch ($match[2]) {
                        case '=' :
                            $replace = " LIKE '%view%' OR action LIKE '%report%'";
                            break;
                        case '!=' :
                        case '<>' :
                            $replace = " NOT LIKE '%view%' AND action NOT LIKE '%report%'";
                            break;
                        default:
                            $replace = '';
                    }
                    break;
                case 'u' :
                    switch ($match[2]) {
                        case '=' :
                            $replace = " LIKE '%update%'";
                            break;
                        case '!=' :
                        case '<>' :
                            $replace = " NOT LIKE '%update%'";
                            break;
                        default:
                            $replace = '';
                    }
                    break;
                case 'd' :
                    switch ($match[2]) {
                        case '=' :
                            $replace = " LIKE '%delete%'";
                            break;
                        case '!=' :
                        case '<>' :
                            $replace = " NOT LIKE '%delete%'";
                            break;
                        default:
                            $replace = '';
                    }
                    break;
                default :
                    $replace = '';
            }
            $return .= $replace;
        }
        return $return;
    }
}

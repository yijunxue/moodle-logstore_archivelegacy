<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Tests the archive legacy log store Privacy API implementation.
 *
 * @package     logstore_archivelegacy
 * @copyright   University of Nottingham, 2018
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use logstore_archivelegacy\privacy\provider;
use core_privacy\local\request\contextlist;
use core_privacy\local\request\writer;

defined('MOODLE_INTERNAL') || die();

/**
 * Tests the archive legacy log store privacy provider class.
 *
 * @package     logstore_archivelegacy
 * @copyright   University of Nottingham, 2018
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group logstore_archivelegacy
 * @group uon
 */
class logstore_archivelegacy_privacy_provider_test extends \core_privacy\tests\provider_testcase {
    use logstore_archivelegacy\local\testing\local_table;

    /**
     * Get the contextlist for a user.
     *
     * @param object $user The user.
     * @return contextlist
     */
    protected function get_contextlist_for_user($user) {
        $contextlist = new contextlist();
        provider::add_contexts_for_userid($contextlist, $user->id);
        return $contextlist->get_contexts();
    }

    /**
     * Test that when a user has no logs that they no contexts are found.
     */
    public function test_user_with_no_logs() {
        global $DB;
        $this->resetAfterTest();
        // Create soem users.
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        // Create a log record for the other user.
        $ctx = context_course::instance(SITEID);
        $record = (object) array(
            'userid' => $otheruser->id,
            'time' => time(),
            'ip' => '127.0.0.1',
            'course' => 0,
            'module' => '',
            'cmid' => 0,
            'action' => 'view',
            'url' => 'url.php?id=1',
            'info' => '',
        );
        $DB->insert_record(self::$tablename, $record);
        // Test no contexts are retrieved.
        $this->assertEquals([], $this->get_contextlist_for_user($user));
    }

    /**
     * Test that we get data for the user when they have a log entry.
     */
    public function test_user_with_log() {
        global $DB;
        $this->resetAfterTest();
        // Create some users.
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        $coursecontext = \context_course::instance($course->id);
        $activity = self::getDataGenerator()->create_module('assign', ['course' => $course->id]);
        $cm = get_coursemodule_from_instance('assign', $activity->id, $course->id);
        $cmcontext = \context_module::instance($cm->id);
        // Create a log record for the users.
        $record = (object) array(
            'userid' => $user->id,
            'time' => time(),
            'ip' => '127.0.0.1',
            'course' => $course->id,
            'module' => 'assign',
            'cmid' => $cm->id,
            'action' => 'view',
            'url' => 'url.php?id=1',
            'info' => '',
        );
        // Logs that should not be returned.
        $DB->insert_record(self::$tablename, $record);
        $record->userid = $otheruser->id;
        $DB->insert_record(self::$tablename, $record);
        $record->userid = $user->id;
        $record->module = '';
        $record->cmid = 0;
        $DB->insert_record(self::$tablename, $record);
        // Test the contexts are retrived.
        $this->assertEquals([$coursecontext, $cmcontext], $this->get_contextlist_for_user($user));
        // Test export.
        $path = [get_string('privacy:path:logs', 'tool_log'), get_string('pluginname', 'logstore_archivelegacy')];
        $approvedcontextlist = new \core_privacy\tests\request\approved_contextlist(
            \core_user::get_user($user->id),
            'logstore_archivelegacy',
            [$cmcontext->id]
        );
        provider::export_user_data($approvedcontextlist);
        $data = writer::with_context($cmcontext)->get_data($path);
        $this->assertCount(1, $data->logs);
    }

    /**
     * Test that we delete the logs for all the users in a context.
     */
    public function test_delete_data_for_all_users_in_context() {
        global $DB;
        $this->resetAfterTest();
        // Create soem users.
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        $coursecontext = \context_course::instance($course->id);
        $activity = self::getDataGenerator()->create_module('assign', ['course' => $course->id]);
        $cm = get_coursemodule_from_instance('assign', $activity->id, $course->id);
        $cmcontext = \context_module::instance($cm->id);
        // Create a log record for the users.
        $record = (object) array(
            'userid' => $user->id,
            'time' => time(),
            'ip' => '127.0.0.1',
            'course' => $course->id,
            'module' => 'assign',
            'cmid' => $cm->id,
            'action' => 'view',
            'url' => 'url.php?id=1',
            'info' => '',
        );
        $DB->insert_record(self::$tablename, $record);
        $record->userid = $otheruser->id;
        $DB->insert_record(self::$tablename, $record);
        // Record that should not be deleted.
        $record->userid = $user->id;
        $record->module = '';
        $record->cmid = 0;
        $DB->insert_record(self::$tablename, $record);
        // Make the call to delete.
        provider::delete_data_for_all_users_in_context($cmcontext);
        // Test the records in the context have been deleted.
        $this->assertEquals(0, $DB->count_records(self::$tablename, ['cmid' => $cm->id]));
        // Test that records in other contexts have not been deleted.
        $this->assertEquals(1, $DB->count_records(self::$tablename));
    }

    /**
     * Test that we delete the logs for a single user in a context.
     */
    public function test_delete_data_for_user() {
        global $DB;
        $this->resetAfterTest();
        // Create soem users.
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        $coursecontext = \context_course::instance($course->id);
        $activity = self::getDataGenerator()->create_module('assign', ['course' => $course->id]);
        $cm = get_coursemodule_from_instance('assign', $activity->id, $course->id);
        $cmcontext = \context_module::instance($cm->id);
        // Create a log record for the users.
        $record = (object) array(
            'userid' => $user->id,
            'time' => time(),
            'ip' => '127.0.0.1',
            'course' => $course->id,
            'module' => 'assign',
            'cmid' => $cm->id,
            'action' => 'view',
            'url' => 'url.php?id=1',
            'info' => '',
        );
        $DB->insert_record(self::$tablename, $record);
        $record->userid = $otheruser->id;
        $DB->insert_record(self::$tablename, $record);
        // Record that should not be deleted.
        $record->userid = $user->id;
        $record->module = '';
        $record->cmid = 0;
        $DB->insert_record(self::$tablename, $record);
        // Make the call to delete.
        $approvedcontextlist = new \core_privacy\tests\request\approved_contextlist(
            \core_user::get_user($user->id),
            'logstore_archive',
            [$cmcontext->id]
        );
        provider::delete_data_for_user($approvedcontextlist);
        // Test the records in the context have been deleted.
        $this->assertEquals(0, $DB->count_records(self::$tablename, ['userid' => $user->id, 'cmid' => $cm->id]));
        // Test that records created by other users have not been deleted.
        $this->assertEquals(1, $DB->count_records(self::$tablename, ['cmid' => $cm->id]));
        // Test that other contexts have not been affected.
        $this->assertEquals(2, $DB->count_records(self::$tablename));
    }
}
